import re

def check_decks(deck,deck_name):
    print("Deck value:"+deck)
    deck_value=deck.strip('{').strip('}')
    #print("Now the Deck_value is "+ deck_value)
    card_list=deck_value.split(',')
    print(card_list)
    no_of_cards=len(card_list)
    print(no_of_cards)
    if no_of_cards==52:
        print("Number of cards check Passed:No of cards in "+deck_name+" is "+str(no_of_cards))
    else: 
        print("Number of cards check  Failed:No of Cards in "+deck_name+" is "+str(no_of_cards))
    unique_card_list=set(card_list)
    if len(card_list) == len(unique_card_list) :
        print("Unique Card Test:Passed")
    else :
        print("Unique Card Test:Failed")
    print(unique_card_list)
    card_split= re.findall(r'(\d+)(\D)', card_list[0])
    print(card_split)
    
def check_prefilled_deck_n(Final_Faceup_cards,Final_Facedown_cards,Final_Hand_cards,Prefilled_Faceup_cards,Prefilled_Facedown_cards,Prefilled_Hand_Cards):
    print("Inside")
    Final_Faceup_cards_deck_split=Final_Faceup_cards.strip('{').strip('}').split(':')
    #print(Final_Faceup_cards_deck_split)
    Final_Faceup_cards_value=Final_Faceup_cards_deck_split[1].split(',')
    print(Final_Faceup_cards_value)
    Prefilled_Faceup_cards_value=Prefilled_Faceup_cards.strip('{').strip('}').split(',')
    print(Prefilled_Faceup_cards_value)
    face_up_subset_check=subset_check(Final_Faceup_cards_value,Prefilled_Faceup_cards_value)
    if face_up_subset_check==0:
        print("Passed:All Prefilled_Faceup_cards are in Final_Faceup_cards")
    else:
        print("Failed: Not all Prefilled_Faceup_cards are in Final_Faceup_cards")

    Final_Hand_cards_deck_split=[]
    Final_Hand_cards_value=[]
    for i in Final_Hand_cards:
         Final_Hand_cards_deck_split_part=i.strip('{').strip('}').split(':')
         Final_Hand_cards_deck_split.append(Final_Hand_cards_deck_split_part)
         #print(Final_Hand_cards_deck_split)
         #print(Final_Hand_cards_deck_split_part[1].split(','))
         Final_Hand_cards_value+=Final_Hand_cards_deck_split_part[1].split(',')
    print(Final_Hand_cards_deck_split)
    print(Final_Hand_cards_value)
    Prefilled_Hand_Cards_value=Prefilled_Hand_Cards.strip('{').strip('}').split(',')
    hand_cards_subset_check=subset_check(Final_Hand_cards_value,Prefilled_Hand_Cards_value)
    if hand_cards_subset_check==0:
        print("Passed:All Prefilled_Hand_Cards are in Final_Hand_cards.")
    else:
        print("Failed: Not all Prefilled_Hand_Cards are in Final_Hand_cards.")


def subset_check(Final_set,Prefilled_set):
    valid=0
    for i in Prefilled_set:
        #print(i)
        if i not in Final_set:
            valid=1
    return valid

def main():
    print("Code Starts Here")
    layout_id='L_1'
    sequence_id='S1'
    print("Setting initial parameters................")
    Final_No_of_Faceup_cards=10
    Final_No_of_Facedown_cards=30
    No_of_Prefilled_Faceup_cards=5
    No_of_Prefilled_Facedown_cards=7
    No_of_Prefilled_Hand_cards=3
    Final_Faceup_cards='{Deck1:12c,10s,12s,6d,2s,13h,8s,7s,1c,13c}'
    Final_Facedown_cards='{Deck1:9s,4d,4c,3c,6s,2d,6c,9d,1h,5d,10h,11s,13d,7c,8c,11d,10c,5s,12h,13s,4s,3s,3d,1d,11h,6h,8h,8d,5h,3h}'
    Final_Hand_cards=['{Deck1:5c,7d,7h,9c,11c,2c,4h,10d,9h,12d,2h,1s}','{Deck2:5d,7h,12h,7d,6d,12d,10s,3c,1s,3h,10c,7s,8d,1c,6h,3d,4s,2h,4c,9s,1h,7c,6s,12s,9c,5s,2c,11h,9d,8h,9h,6c,4d,13c,13h,10d,5c,3s,4h,11s,12c,13s,5h,11d,10h,1d,2d,11c,8c,8s,13d,2s}']
    Prefilled_Faceup_cards='{12c,10s,12s,6d,2s}'
    Prefilled_Facedown_cards='{9s,4d,4c,3c,6s,2d,6c}'
    Prefilled_Hand_Cards='{5c,7d,7h}'
    Deck1='{12c,10s,12s,6d,2s,9s,4d,4c,3c,6s,2d,6c,5c,7d,7h,13h,8s,7s,1c,13c,9d,1h,5d,10h,11s,13d,7c,8c,11d,10c,5s,12h,13s,4s,3s,3d,1d,11h,6h,8h,8d,5h,3h,9c,11c,2c,4h,10d,9h,12d,2h,1s}'
    Deck2='{5d,7h,12h,7d,6d,12d,10s,3c,1s,3h,10c,7s,8d,1c,6h,3d,4s,2h,4c,9s,1h,7c,6s,12s,9c,5s,2c,11h,9d,8h,9h,6c,4d,13c,13h,10d,5c,3s,4h,11s,12c,13s,5h,11d,10h,1d,2d,11c,8c,8s,13d,2s}'
    Deck3='{6d,3d,5h,7c,9h,12s,5c,1s,10c,5d,2s,9s,13d,13h,5s,12c,9d,4c,3c,11c,1d,9c,4h,10h,13c,8h,11d,1h,8c,11s,3h,13s,7h,12d,6c,4s,10d,3s,6h,4d,12h,1c,2c,11h,7d,6s,10s,2d,8d,7s,2h,8s}'
    print("Initial Parameters set.")
    #check_decks(Deck1,'Deck1')
    check_prefilled_deck_n(Final_Faceup_cards,Final_Facedown_cards,Final_Hand_cards,Prefilled_Faceup_cards,Prefilled_Facedown_cards,Prefilled_Hand_Cards)



if __name__=="__main__":
	main() 
