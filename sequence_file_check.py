import re

def check_decks(deck,deck_name):
    print("Deck value:"+deck)
    deck_value=deck.strip('{').strip('}')
    #print("Now the Deck_value is "+ deck_value)
    card_list=deck_value.split(',')
    print(card_list)
    no_of_cards=len(card_list)
    print(no_of_cards)
    if no_of_cards==52:
        print("Number of cards check Passed:No of cards in "+deck_name+" is "+str(no_of_cards))
    else: 
        print("Number of cards check  Failed:No of Cards in "+deck_name+" is "+str(no_of_cards))
    unique_card_list=set(card_list)
    if len(card_list) == len(unique_card_list) :
        print("Unique Card Test:Passed")
    else :
        print("Unique Card Test:Failed")
    print(unique_card_list)
    card_split= re.findall(r'(\d+)(\D)', card_list[0])
    print(card_split)
    
def check_prefilled_deck_n(Final_Faceup_cards,Final_Facedown_cards,Final_Hand_cards,Prefilled_Faceup_cards,Prefilled_Facedown_cards,Prefilled_Hand_Cards):
    #print("Inside")
    Final_Faceup_cards_deck_split=[]
    Final_Faceup_cards_value=[]
    for i in Final_Faceup_cards:
        Final_Faceup_cards_deck_split_part=i.strip('{').strip('}').split(':')
        Final_Faceup_cards_deck_split.append(Final_Faceup_cards_deck_split_part)
        Final_Faceup_cards_value+=Final_Faceup_cards_deck_split_part[1].split(',')
    #print(Final_Faceup_cards_deck_split)
    #print(Final_Faceup_cards_value)    
    Prefilled_Faceup_cards_value=Prefilled_Faceup_cards.strip('{').strip('}').split(',')
    #print(Prefilled_Faceup_cards_value)
    face_up_subset_check=subset_check(Final_Faceup_cards_value,Prefilled_Faceup_cards_value)
    if face_up_subset_check==0:
        print("Passed:All Prefilled_Faceup_cards are in Final_Faceup_cards")
    else:
        print("Failed: Not all Prefilled_Faceup_cards are in Final_Faceup_cards")

    Final_Facedown_cards_deck_split=[]
    Final_Facedown_cards_value=[]
    for i in Final_Facedown_cards:
        Final_Facedown_cards_deck_split_part=i.strip('{').strip('}').split(':')
        Final_Facedown_cards_deck_split.append(Final_Facedown_cards_deck_split_part)
        Final_Facedown_cards_value+=Final_Facedown_cards_deck_split_part[1].split(',')
    Prefilled_Facedown_cards_value=Prefilled_Facedown_cards.strip('{').strip('}').split(',')
    face_down_subset_check=subset_check(Final_Facedown_cards_value,Prefilled_Facedown_cards_value)
    if face_down_subset_check==0:
        print("Passed:All Prefilled_Facedown_cards are in Final_Facedown_cards")
    else:
        print("Failed: Not all Prefilled_Facedown_cards are in Final_Facedown_cards")

 
    Final_Hand_cards_deck_split=[]
    Final_Hand_cards_value=[]
    for i in Final_Hand_cards:
         Final_Hand_cards_deck_split_part=i.strip('{').strip('}').split(':')
         Final_Hand_cards_deck_split.append(Final_Hand_cards_deck_split_part)
         #print(Final_Hand_cards_deck_split)
         #print(Final_Hand_cards_deck_split_part[1].split(','))
         Final_Hand_cards_value+=Final_Hand_cards_deck_split_part[1].split(',')
    #print(Final_Hand_cards_deck_split)
    #print(Final_Hand_cards_value)
    Prefilled_Hand_Cards_value=Prefilled_Hand_Cards.strip('{').strip('}').split(',')
    hand_cards_subset_check=subset_check(Final_Hand_cards_value,Prefilled_Hand_Cards_value)
    if hand_cards_subset_check==0:
        print("Passed:All Prefilled_Hand_Cards are in Final_Hand_cards.")
    else:
        print("Failed: Not all Prefilled_Hand_Cards are in Final_Hand_cards.")


def subset_check(Final_set,Prefilled_set):
    valid=0
    for i in Prefilled_set:
        #print(i)
        if i not in Final_set:
            valid=1
    return valid

def deck_full_check(Final_Faceup_cards,Final_Facedown_cards,Final_Hand_cards):
    print("Inside")
    Final_Faceup_cards_deck_split=[]
    for i in Final_Faceup_cards:
        Final_Faceup_cards_deck_split_part=i.strip('{').strip('}').split(':')
        Final_Faceup_cards_deck_split.append(Final_Faceup_cards_deck_split_part)
    
    Final_Facedown_cards_deck_split=[]
    for i in Final_Facedown_cards:
        Final_Facedown_cards_deck_split_part=i.strip('{').strip('}').split(':')
        Final_Facedown_cards_deck_split.append(Final_Facedown_cards_deck_split_part)

    Final_Hand_cards_deck_split=[]
    for i in Final_Hand_cards:
        Final_Hand_cards_deck_split_part=i.strip('{').strip('}').split(':')
        Final_Hand_cards_deck_split.append(Final_Hand_cards_deck_split_part)

    print(Final_Faceup_cards_deck_split)
    print(Final_Facedown_cards_deck_split)
    print(Final_Hand_cards_deck_split)
    deck_list=[]
    for i in Final_Faceup_cards_deck_split:
        Final_Faceup_card_deck_name=i[0]
        Final_Faceup_cards_deck_value=i[1].split(',')
        deck_tuple=Final_Faceup_card_deck_name+':'+str(Final_Faceup_cards_deck_value)
        deck_list.append(deck_tuple)
    #print(deck_tuple)
    #print(deck_list)
    
    for i in Final_Facedown_cards_deck_split:
        Final_Facedown_cards_deck_name=i[0]
        Final_Facedown_cards_deck_value=i[1].split(',')
        deck_tuple=Final_Facedown_cards_deck_name+':'+str(Final_Facedown_cards_deck_value)
        #print(deck_tuple)
        for decks in deck_list:
            main_deck_name=decks.split(':')[0]
            main_deck_values=decks.split(':')[1].strip('[').strip(']').replace("'","").split(',')
            print(main_deck_values)
            if main_deck_name==Final_Facedown_cards_deck_name:
                main_deck_values+=Final_Facedown_cards_deck_value
                #print("deck_list Before Removal:"+str(deck_list))
                deck_list.remove(decks)
                #print("deck_list After Removal:"+str(deck_list))
                new_deck_list_value=main_deck_name+":"+str(main_deck_values)
                deck_list.append(new_deck_list_value)
                #print("deck_list After Modification :"+str(deck_list))
            else:
                new_deck_list_value=Final_Facedown_cards_deck_name+":"+str(Final_Facedown_cards_deck_value)
                deck_list.append(new_deck_list_value)
                #print("deck_list After Modification :"+str(deck_list))
                 
    for i in Final_Hand_cards_deck_split:
       Final_Hand_cards_deck_name=i[0]   
       Final_Hand_cards_deck_value=i[1].split(',')
       #print("Outer "+Final_Hand_cards_deck_name)
       #print(Final_Hand_cards_deck_value)
       for decks in deck_list:
           main_deck_name=decks.split(':')[0]
           main_deck_values=decks.split(':')[1].strip('[').strip(']').replace("'","").replace(' ','').split(',')                 
           #print("Inner"+main_deck_name)
           #print("main_deck_values"+str(main_deck_values))
           if main_deck_name==Final_Hand_cards_deck_name:
               if main_deck_values==Final_Hand_cards_deck_value:
                   main_deck_values=[]
               main_deck_values+=Final_Hand_cards_deck_value
               #print("deck_list Before Removal:"+str(deck_list))
               deck_list.remove(decks)
               #print("deck_list After Removal:"+str(deck_list))
               new_deck_list_value=main_deck_name+":"+str(main_deck_values)
               deck_list.append(new_deck_list_value)
               #print("deck_list After Modification1 :"+str(deck_list))
           else:
               new_deck_list_value=Final_Hand_cards_deck_name+":"+str(Final_Hand_cards_deck_value)
               deck_list.append(new_deck_list_value)
               #print("deck_list After Modification2 :"+str(deck_list))
    print("Final Deck List:")
    print(deck_list)
    fulfill_deck(deck_list)

def fulfill_deck(deck_list):
    for deck in deck_list:
        deck_name=deck.split(':')[0]
        deck_value=deck.split(':')[1].strip('[').strip(']').replace("'","").replace(' ','').split(',')
        if len(deck_value)==52:
            print("Passed: Sequence Deck: "+deck_name+" has "+str(len(deck_value))+" cards.")
        else:
            print("Failed: Sequence Deck: "+deck_name+" has "+str(len(deck_value))+" cards.")
        unique_deck_value=set(deck_value)
        if len(deck_value)==len(unique_deck_value):
            print("Passed: Sequence Deck: "+deck_name+" has unique cards.")
        else:
            print("Failed: Sequence Deck: "+deck_name+" has duplicate cards.")

def validation_checkpoints(Final_Hand_cards):
    #print(Final_Hand_cards)
    Final_Hand_cards_deck_split=[]
    Final_Hand_cards_deck_values=[]
    card_split=[]
    for i in Final_Hand_cards:
        Final_Hand_cards_deck_split_part=i.strip('{').strip('}').split(':')
        Final_Hand_cards_deck_values+=Final_Hand_cards_deck_split_part[1].split(',')
        Final_Hand_cards_deck_split.append(Final_Hand_cards_deck_split_part)
    print(Final_Hand_cards_deck_values)
    #print(len(Final_Hand_cards_deck_values))
    for i in range(0,len(Final_Hand_cards_deck_values)):
        card_split+= re.findall(r'(\d+)(\D)', Final_Hand_cards_deck_values[i])
    #print(card_split)
    
    print("Validation Condition for Hand Cards (1):Consecutive Cards (asc and desc)")
    asc_inc_consective=[0]
    asc_cnt=0
    for i in range(1,len(card_split)-1):
        if (( int(card_split[i][0])==int(card_split[i-1][0])+1 ) or ( int(card_split[i][0])==1 and int(card_split[i-1][0])==13 )):
            asc_cnt=asc_cnt+1
        else: 
            asc_cnt=0
        asc_inc_consective.append(asc_cnt)
    #print(asc_inc_consective)

    desc_inc_consective=[0]
    desc_int=0
    for i in range(1,len(card_split)-1):
        if (( int(card_split[i][0])==int(card_split[i-1][0])-1 ) or ( int(card_split[i][0])==13 and int(card_split[i-1][0])==1 )):
            desc_cnt=desc_cnt+1
        else:
            desc_cnt=0
        desc_inc_consective.append(desc_cnt)
    #print(desc_inc_consective)
    
    con_asc_present=0
    con_desc_present=0
    for i in range(0,len(asc_inc_consective)-1):
        if int(asc_inc_consective[i])==2:
            con_asc_present=1
            print("Consective Cards Found(ASC):")
            print("Position: "+str(i)+": Card_value: "+Final_Hand_cards_deck_values[i])
            print("Position: "+str(i-1)+": Card_value: "+Final_Hand_cards_deck_values[i-1])
            print("Position: "+str(i-2)+": Card_value: "+Final_Hand_cards_deck_values[i-2])

        if int(desc_inc_consective[i])==2:
            con_desc_present=1
            print("Consective Cards Found(DESC):")
            print("Position: "+str(i)+": Card_value: "+Final_Hand_cards_deck_values[i])
            print("Position: "+str(i-1)+": Card_value: "+Final_Hand_cards_deck_values[i-1])
            print("Position: "+str(i-2)+": Card_value: "+Final_Hand_cards_deck_values[i-2])

    if con_asc_present==0 and con_desc_present==0:
        print("No Consective Hand Cards Present")
    
    print("Validation Condition for Hand Cards (2): Consecutive Repetitions")
    #print("Sets of 2:")
    set_2=[]
    set_3=[]
    set_4=[]
    set_2_bool1=0
    set_3_bool1=0
    set_4_bool1=0
    for i in range(0,len(card_split)-1):
        if i<len(card_split)-3 and card_split[i][0]==card_split[i+2][0] and card_split[i+1][0]==card_split[i+3][0]:
            set_2.append(1)
        else:
            set_2.append(0)
        if i<len(card_split)-5 and card_split[i][0]==card_split[i+3][0] and card_split[i+1][0]==card_split[i+4][0] and card_split[i+2][0]==card_split[i+5][0]:
            set_3.append(1)
        else:
            set_3.append(0)
        if i<len(card_split)-7 and card_split[i][0]==card_split[i+4][0] and card_split[i+1][0]==card_split[i+5][0] and card_split[i+2][0]==card_split[i+6][0] and card_split[i+3][0]==card_split[i+7][0]:
            set_4.append(1)
        else:
            set_4.append(0)
    for i in range(0,len(card_split)-1):
        if set_2[i]==1:
            print("Consecutive Repetitions for Group 2 present ")
            print("Position ("+str(i)+","+str(i+1)+","+str(i+2)+","+str(i+3)+") Cards: ("+Final_Hand_cards_deck_values[i]+","+Final_Hand_cards_deck_values[i+1]+","+Final_Hand_cards_deck_values[i+2]+","+Final_Hand_cards_deck_values[i+3]+")")
            set_2_bool1=1
        if set_3[i]==1:
            print("Consecutive Repetitions for Group 3 present ")
            print("Position ("+str(i)+","+str(i+1)+","+str(i+2)+","+str(i+3)+","+str(i+4)+","+str(i+5)+") Cards: ("+Final_Hand_cards_deck_values[i]+","+Final_Hand_cards_deck_values[i+1]+","+Final_Hand_cards_deck_values[i+2]+","+Final_Hand_cards_deck_values[i+3]+","+Final_Hand_cards_deck_values[i+4]+","+Final_Hand_cards_deck_values[i+5]+")")
            set_3_bool1=1
        if set_4[i]==1:
            print("Consecutive Repetitions for Group 4 present ")
            print("Position ("+str(i)+","+str(i+1)+","+str(i+2)+","+str(i+3)+","+str(i+4)+","+str(i+5)+","+str(i+6)+","+str(i+7)+") Cards: ("+Final_Hand_cards_deck_values[i]+","+Final_Hand_cards_deck_values[i+1]+","+Final_Hand_cards_deck_values[i+2]+","+Final_Hand_cards_deck_values[i+3]+","+Final_Hand_cards_deck_values[i+4]+","+Final_Hand_cards_deck_values[i+5]+","+Final_Hand_cards_deck_values[i+6]+","+Final_Hand_cards_deck_values[i+7]+")")
            set_4_bool1=1
    
    print("Validation Condition for Hand Cards (3): Similar Consecutives")
    inc_consective=[]
    cnt=0
    for i in range(0,len(card_split)-1):
        if i<len(card_split)-2 and int(card_split[i][0])==int(card_split[i+1][0])==int(card_split[i+2][0]):
           cnt=1
           #print(card_split[i][0])
        else:
           cnt=0
        inc_consective.append(cnt)
    #print(inc_consective)
    for i in range(0,len(inc_consective)-1):
        if inc_consective[i]==1:
            print("Similar consectives found")
            print("Positions: ("+str(i)+","+str(i+1)+","+str(i+2)+") Cards :("+Final_Hand_cards_deck_values[i]+","+Final_Hand_cards_deck_values[i+1]+","+Final_Hand_cards_deck_values[i+2]+")")
        

def main():
    print("Code Starts Here")
    layout_id='L_1'
    sequence_id='S1'
    print("Setting initial parameters.........")
    Final_No_of_Faceup_cards=10
    Final_No_of_Facedown_cards=30
    No_of_Prefilled_Faceup_cards=5
    No_of_Prefilled_Facedown_cards=7
    No_of_Prefilled_Hand_cards=3
    Final_Faceup_cards=['{Deck1:12c,10s,12s,6d,2s,13h,8s,7s,1c,13c}']
    Final_Facedown_cards=['{Deck1:9s,4d,4c,3c,6s,2d,6c,9d,1h,5d,10h,11s,13d,7c,8c,11d,10c,5s,12h,13s,4s,3s,3d,1d,11h,6h,8h,8d,5h,3h}']
    Final_Hand_cards=['{Deck1:5c,7d,7h,9c,11c,2c,4h,10d,9h,12d,2h,1s}','{Deck2:5d,7h,12h,7d,6d,12d,10s,3c,1s,3h,10c,7s,8d,1c,6h,3d,4s,2h,4c,9s,1h,7c,6s,12s,9c,5s,2c,11h,9d,8h,9h,6c,4d,13c,13h,10d,5c,3s,4h,11s,12c,13s,5h,11d,10h,1d,2d,11c,8c,8s,13d,2s}']
    Consective_Test_Final_Hand_cards=['{Deck1:5c,7d,7h,9c,11c,2c,4h,10d,9h,10d,9h,1s}','{Deck2:9d,10h,9h,1d,6d,12d,10s,3c,3s,3h,10c,7s,8d,1c,6h,3d,4s,2h,4c,9s,1h,7c,6s,12s,9c,5s,2c,11h,9d,8h,9h,6c,4d,13c,13h,10d,5c,3s,10h,5s,3c,13s,5h,11d,10h,1d,2d,13c,2c,11s,2d,11s}']
    Prefilled_Faceup_cards='{12c,10s,12s,6d,2s}'
    Prefilled_Facedown_cards='{9s,4d,4c,3c,6s,2d,6c}'
    Prefilled_Hand_Cards='{5c,7d,7h}'
    Deck1='{12c,10s,12s,6d,2s,9s,4d,4c,3c,6s,2d,6c,5c,7d,7h,13h,8s,7s,1c,13c,9d,1h,5d,10h,11s,13d,7c,8c,11d,10c,5s,12h,13s,4s,3s,3d,1d,11h,6h,8h,8d,5h,3h,9c,11c,2c,4h,10d,9h,12d,2h,1s}'
    Deck2='{5d,7h,12h,7d,6d,12d,10s,3c,1s,3h,10c,7s,8d,1c,6h,3d,4s,2h,4c,9s,1h,7c,6s,12s,9c,5s,2c,11h,9d,8h,9h,6c,4d,13c,13h,10d,5c,3s,4h,11s,12c,13s,5h,11d,10h,1d,2d,11c,8c,8s,13d,2s}'
    Deck3='{6d,3d,5h,7c,9h,12s,5c,1s,10c,5d,2s,9s,13d,13h,5s,12c,9d,4c,3c,11c,1d,9c,4h,10h,13c,8h,11d,1h,8c,11s,3h,13s,7h,12d,6c,4s,10d,3s,6h,4d,12h,1c,2c,11h,7d,6s,10s,2d,8d,7s,2h,8s}'
    print("Initial Parameters set.")
    check_decks(Deck1,'Deck1')
    check_prefilled_deck_n(Final_Faceup_cards,Final_Facedown_cards,Final_Hand_cards,Prefilled_Faceup_cards,Prefilled_Facedown_cards,Prefilled_Hand_Cards)
    deck_full_check(Final_Faceup_cards,Final_Facedown_cards,Final_Hand_cards)
    validation_checkpoints(Consective_Test_Final_Hand_cards)

if __name__=="__main__":
    main() 
